package bagend

import (
	"fmt"
	"runtime"
)


// TODO always panics with passed message
// It returns any type so that it can be used in not implemented functions that
// return values
func TODO(msg string) interface{} {
	_, file, no, ok := runtime.Caller(1)
	var panicmsg string
	if ok {
		panicmsg = fmt.Sprintf("TODO %s (%s#%d)", msg, file, no)
	} else {
		panicmsg = fmt.Sprintf("TODO %s", msg)
	}
	panic(panicmsg)
}

// Assert accepts a condition and a message. It panics with the message if the
// condition is false
func Assert(cond bool, msg string) {
	if !cond {
		_, file, no, ok := runtime.Caller(1)
		var panicmsg string
		if ok {
			panicmsg = fmt.Sprintf("assertion in %s#%d failed", file, no)
		} else {
			panicmsg = fmt.Sprintf("assertion failed")
		}
		if msg != "" {
			panicmsg = fmt.Sprintf("%s (%s)", panicmsg, msg)
		}
		panic(panicmsg)
	}
}

package bagend

import (
	"testing"
)

func TestTodo(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Errorf("expected a panic\n")
		}
	}()

	TODO("test")
}

func TestAssertTrue(t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			t.Errorf("didn’t expect a panic\n")
		}
	}()

	Assert(2+2 == 4, "2+2 must be equal 4")
}

func TestAssertFalse(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Errorf("expected a panic\n")
		}
	}()

	Assert(2+2 == 5, "2+2 must be equal 4")
}

func a() string {
	return TODO("a").(string)
}
